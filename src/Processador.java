
//Essa classe é responsável por simular o comportamento de um processador, buscando informações na memória
//e cuidando da temporização da execução
public class Processador {

    //variável que guarda o tempo acumulado da execução até o momento atual
    private int tempoAcumulado;

    //variável que indica qual processo está sendo executado atualmente pela CPU
    private Processo processoAtual;

    //variável que indica qual instrução está sendo executada atualmente pelo processo atual
    private String[] instrucaoAtual;

    //Variável que guarda a TLB a qual a CPU fará as consultas
    private TLB tlb;

    //cria um processador dado um tamanho da tlb
    public Processador(int tamanhoQuadro, int tamanhoTLB){

        //instancia todas as variáveis
        tempoAcumulado = 0;
        processoAtual = null;
        instrucaoAtual = null;
        tlb = new TLB(tamanhoTLB, tamanhoQuadro);

    }

    //função que busca a próxima instrução no processo atual e armazena na variável instrução atual
    public void buscaInstrucao(){

        //se existe um processo sendo executado
        if(processoAtual != null){
            //busca a próxima instrução do processo e armazena na instrução atual
            instrucaoAtual = processoAtual.getNextInstruction();
        }else{
            instrucaoAtual[0] = null;
        }

    }

    //função que busca a entrada referente a uma pagina na TLB, retorna null caso de tlb miss
    public ElementoTabelaPaginas buscaTlbAtual(String nPagina){

        return tlb.getElem(nPagina, processoAtual.getId());

    }

    public ElementoTabelaPaginas buscaTlb(String nPagina, int id){

        return tlb.getElem(nPagina, id);

    }

    //função que busca um elemento da TP em memoria
    public ElementoTabelaPaginas buscaTP(String nPagina){

        //pega a tabela de páginas do processo
        TabeladePaginas tp = processoAtual.getTp();
        //transforma o número da página em um inteiro na base 10
        int pagina = Integer.parseInt(nPagina, 2);
        //retorna o elemento referente a esse numero da pagina
        return tp.getElem(pagina);

    }

    //adiciona um elemento da tlb na tlb
    public void addtlb(int id, String npagina, ElementoTabelaPaginas elem){
        tlb.addElem(id, npagina, elem);
    }

    //Função que calcula o endereço físico de um dado acesso a memoria
    public String calcEndReal(String endLogico, String nQuadro, int bitsnPagina){

        //retira o offset do endereço
        String offset =  endLogico.substring(bitsnPagina);
        //retorna o numero do quadro + o offset
        return nQuadro + offset;

    }

    //função que coloca um processo em execução
    public void setProcessoAtual(Processo processoAtual){
        this.processoAtual = processoAtual;
    }

    //função que retorna a instrução atual, de modo que o controlador possa tomar as devidas decisões
    public String[] getInstrucaoAtual(){
        return instrucaoAtual;
    }

    //retorna o tempo acumulado que se passou até o momento
    //o tempo acumulado é referente a quantidade de loops que foram feitos
    public int getTempoAcumulado(){
        return tempoAcumulado;
    }

    //retorna o processo atual
    public Processo getProcessoAtual(){
        return processoAtual;
    }

    //Função que atualiza a variável tempoAcumulado e deve ser chamada a cada loop
    public void updateTime(){
        tempoAcumulado ++;
    }

    //retorna toda a tlb, usada primariamente para a impressao de informacoes do sistema
    public ElementoTlb[] getElementosTlb(){
        return tlb.getElementosTlb();
    }

    //funções que apenas refletem as funcionalidades da tlb um nível acima
    public void setValid0(String nPagina, int id){
        tlb.setValid0(nPagina, id);
    }

    public void invalidaProcesso(int id){
        tlb.invalidaProcesso(id);
    }

}
