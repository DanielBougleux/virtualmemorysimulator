
import java.io.*;
import java.util.ArrayList;

//Esta classe representa um processo e as informações necessárias do mesmo
public class Processo {

    //Program Counter, indica qual é o índice da próxima instrução a ser executada
    private int pc;

    //tamanho do processo, em MB
    private int tamanhoproc;

    //Variavel para o tempo em que um processo fica bloqueado
    private int tempoCastigo;

    //id do processo
    private int id;

    //A tabela de páginas do processo
    private TabeladePaginas tp;

    //O status do processo(executando, bloqueado, suspenso, etc...)
    private String status;

    //Guarda o tempo referente a ultima vez que o processo trocou de estado
    //essa variável é utilizada para comparar quem está bloqueado a mais tempo
    private long ultimaTroca;

    //vetor de instruções que definem a execução de um processo
    private ArrayList<String[]> intrucoes;

    //guarda a quantidade de instruções do processo
    private int qntInst;

    //variável que indica quantas vezes ocorreu pagefault para o processo
    private int pageFault;

    //variável que indica quanto de swap o processo está gastando
    private int swap;

    //guarda o tamanho da tabela de paginas do processo
    private int tamanhotp;

    //Guarda a quantidade de bits do endereco logico que eh referente ao numero da pagina
    private int bitsNPagina;

    //Estes dois atributos sao utilizados para o momento em que um processo sai de bloqueado e vai para pronto
    //VAriavel para saber qual instrucao e a proxima apos sair de BLOQUEADO
    private String[] instrucaoPosBloqueio;
    //essa variável indica que um processo estava bloqueado e foi para pronto, se for true a próxima instru
    //ção a ser executada deve ser a instrução pos bloqueio
    private boolean estavaBloqueado;

    //cria um processo através de um arquivo que contém as suas instruções, e define um id para o mesmo.
    //além disso define um tamanho para o processo em megabytes e recebe um tamanho do quadro para
    //calcular o tamanho da tabela de páginas
    //O erro do tamanho do quadro já deve ser tratado na criação da memória principal
    public Processo (String arquivo, int id, int tamanhoP, int tamanhoQuadro, int bitsNPagina){

        //incializa as variáveis com as informações dadas
        this.id = id;
        this.status = "novo";
        this.ultimaTroca = System.currentTimeMillis();
        this.intrucoes = new ArrayList<>();
        this.pc = 0;
        instrucaoPosBloqueio = null;
        estavaBloqueado = false;
        this.qntInst = 0;
        pageFault = 0;
        this.tamanhoproc = tamanhoP;

        //calcula o tamanho do processo em kbytes e inicializa uma variável que vai guardar
        //o tamanho da tabela de páginas
        int temp = tamanhoP * 1024, tamanhotp;

        //verifica quantos quadros são necessários para englobar todo o processo
        if(temp % tamanhoQuadro == 0){
            //se o tamanho do processo em kb for multiplo do tamanho do quadro, apenas efetua a divisão
            tamanhotp = temp / tamanhoQuadro;
        }else {
            //se não for multiplo efetua a divisão e soma 1
            tamanhotp = (temp/tamanhoQuadro) + 1;
        }

        this.bitsNPagina = bitsNPagina;

        //guarda o tamanho da tabela de páginas e cria a tabela de páginas
        this.tamanhotp = tamanhotp;
        this.tp = new TabeladePaginas(tamanhotp, id);

        //executa o procedimento de abrir o arquivo que contém as instruções
        InputStream is;

        try {
            //tenta abrir o arquivo
            is = new FileInputStream(arquivo);
        }catch (FileNotFoundException e){
            //caso o arquivo não exista, aborta a execução
            System.out.println("Arquivo não encontrado, abortando a execuçãp...");
            e.printStackTrace();
            System.exit(1);
            return;
        }

        //executa o procedimento necessário para ler uma linha de cada vez
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);

        //cria uma variável auxiliar que irá guardar a linha
        String aux;
        //faz enquanto não houver um break
        while (true){
            try {
                //tenta ler uma linha do arquivo
                aux = br.readLine();
                //se for null é por que o arquivo acabou, então interrompe o loop
                if (aux == null) break;
                //separa cada campo da instrução e adiciona no vetor de instrucoes
                this.intrucoes.add(aux.split(" "));
                //adiciona um na quantidade de instruções
                this.qntInst ++;
            }catch (IOException i){
                //se houver um erro ao ler o arquivo aborta a execução
                System.out.println("Erro ao ler arquivo");
                i.printStackTrace();
                System.exit(1);
            }
        }

    }

    //Busca a próxima instrução e incrementa o pc, retorna nulo caso o processo tenha terminado
    public String[] getNextInstruction(){


        //verifica se o processo estava bloqueado
        if (estavaBloqueado){
            //se o processo estava bloqueado, coloca o estava bloqueado em false e retorna a isntru
            //ção pos bloqueio
            estavaBloqueado = false;
            return instrucaoPosBloqueio;
        }

        //verifica se ainda há alguma instrução a ser executada
        if (pc < qntInst){

            //se o processo não estava bloqueado guarda a instrução de ond eo pc aponta
            String[] aux = this.intrucoes.get(pc);

            //incrementa pc
            pc++;
            //retorna a instrução
            return aux;
        }
        //se não houver instrução a ser executada retorna null
        return null;
    }

    //todos os getters e setters necessários para fazer a comunicação com as outras classes

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public TabeladePaginas getTp(){
        return tp;
    }

    public long getTempoUltimaTroca(){
        return System.currentTimeMillis() - this.ultimaTroca;
    }

    public void setUltimaTroca(long ultimaTroca){
        this.ultimaTroca = ultimaTroca;
    }

	public int getPageFault() {
		return pageFault;
	}

	public void setPageFault(int pageFault) {
		this.pageFault = pageFault;
	}

	public int getSwap() {
		return swap;
	}

	public void setSwap(int swap) {
		this.swap = swap;
	}

	public int getTamanhoproc() {
		return tamanhoproc;
	}

	public int getTamanhotp() {
		return tamanhotp;
	}

	public int getBitsNPagina() {
		return bitsNPagina;
    }
    
    public void setPc(int pc){
        this.pc = pc;
    }
    
    public int getPc(){
        return pc;
    }

    public void setTempoCastigo(int t){
        this.tempoCastigo = t;
    }

    public int getTempoCastigo(){
        return tempoCastigo;
    }

    public boolean getEstavaBloquead(){
        return estavaBloqueado;
    }

    public void setEstavaBloqueado(boolean e){
        estavaBloqueado = e;
    }

    public void setInstrucaoPosBloqueio(String[] instru){
        instrucaoPosBloqueio = instru;
    }
}
