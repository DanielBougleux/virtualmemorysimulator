import java.util.ArrayList;

public class GerenciadordeMemoria {

    //vetor que guarda todos os processos que estão no sistema
    private ArrayList<Processo> processos;

    //fila de todos os quadros que a memoria possui, de ordem decrescente em tempo que foi acessado
    //esta fila serve de apoio para o LRU, o quadro que será substituido é o elemento 0
    private ArrayList<String> filaQuadros;

    //vetor de bitu para apoio a poltica de substituição do clock
    private int[] bitU;

    //"ponteiro do relógio" do algoritmo de substituição clock
    private int ponteiroclock;

    //Guarda a memória principal, para ter acesso aos quadros
    private MemoriaPrincipal mp;

    //guarda o disco para ter acesso a ele
    private Disco disco;


    public GerenciadordeMemoria(int tamQuadro, int tamMemoria, Disco disco){

        //inicializa as variáveis
        this.disco = disco;
        ponteiroclock = 0;
        int aux = tamMemoria * 1024;
        int nQuadros = aux / tamQuadro;
        processos = new ArrayList<>();
        filaQuadros = new ArrayList<>();
        bitU = new int[nQuadros];
        mp = new MemoriaPrincipal(tamMemoria, tamQuadro);

        //executa um for que preenche os vetores de apoio aos algoritmos de substituição
        int i;
        for(i = 0; i < nQuadros; i++){

            //adiciona um inteiro que i que representa o quadro de numero i
            filaQuadros.add(Integer.toBinaryString(i));
            //coloca todos os bits do vetor de clock para 0
            bitU[i] = 0;

        }

    }

    //metodo para a atualizacao da substituicao LRU, deve ser chamado toda vez que um quadro for acessado
    //recebe o número do quadro que foi acessado
    public void update(String nquadro){

        //transforma no número do quadro em um índice
        int indice = Integer.parseInt(nquadro, 2);
        //seta o bit u para 1, indicando que aquele quadro foi acessado
        bitU[indice] = 1;

        //itera sobre a fila de quadros, procurando o quadro que foi acessado
        for(String s : filaQuadros){
            if(s.equalsIgnoreCase(nquadro)){
                //quando acha o quadro
                //Acha o index do quadro
                int index = filaQuadros.indexOf(s);
                //guarda o quadro em uma variável temporária
                String temp = filaQuadros.get(index);
                //O tiro da posicao atual
                filaQuadros.remove(index);
                //O coloco no final da fila
                filaQuadros.add(temp);
                break;
            }
        }

    }

    //algoritmo da substuição LRU, retorna o quadro que deve ser substituído
    public String subLRU(){

        //o quadro que deve ser substituido é sempre o primeiro da fila.
        //tira o quadro da fila, coloca no final e retorna o quadro
        String temp = filaQuadros.get(0);
        filaQuadros.remove(0);
        filaQuadros.add(temp);
        return temp;

    }

    //algoritmo de substituição relógio com bit u, retorna o numero do quadro a ser substituido
    public String subClock(){

        //executa um for que passa pelo vetor de bits aplicando o algoritmo
        int max = bitU.length, i;
        for(i = ponteiroclock; i < max; i++){

            //sempre que achar um 1 coloca um 0
            if(bitU[i] == 1){
                bitU[i] = 0;
            }else{
                //se achar um 0, substitui esse
                ponteiroclock = i;
                return Integer.toBinaryString(i);
            }
            if(i == max - 1){
                i = -1;
            }
        }

        return null;
    }

    //adiciona um processo no sistema
    public void adicionaProcesso(Processo p){
        processos.add(p);
    }

    //verifica se o sistema tem memória principal suficiente para trazer quatro quadros para a memória
    public boolean memCheia(){
        return mp.getMemDisp() < mp.getTamanhoQuadro() * 4;
    }

    //retorna o vetor de processos
    public ArrayList<Processo> getProcessos(){
        return processos;
    }

    //retorna os quadros disponiveis da mp, metodo feito primariamente para ajudar na impressao das informacoes do sistema
    public ArrayList<String> getQuadrosDisp() {
        return mp.getQuadrosDisp();
    }

    //retorna os quadros da mp, metodo feito primariamente para ajudar na impressao das informacoes do sistema
    public String[] getQuadros(){
        return mp.getQuadros();
    }

    //aloca os quadros iniciais para um dado processo que não está em memória
    public void alocaQuadrosIniciais(int id){

        //procura o processo no vetor de processos e salva em uma variável auxiliar
        Processo aux = null;
        for(Processo x : processos){
            if(x.getId() == id){
                aux = x;
                break;
            }
        }


        int i;
        //salva a tabela de páginas do processo para atualiza-la
        TabeladePaginas tp = aux.getTp();

        //Pega o vetor de quadros disponíveis
        ArrayList<String> temp = mp.getQuadrosDisp();

        //faz um for pra alocar 4 vezes
        for(i = 0; i < 4; i++){

            //pega o primeiro quadro disponível
            String nquadro = temp.get(0);

            //transforma o número do quadro em um inteiro
            int j = Integer.parseInt(nquadro, 2);

            //aloca o quadro na memória
            mp.alocaQuadro(j, aux.getId() + " " + Integer.toBinaryString(i));

            //seta o bit de presença da entrada referente a pagina pra 1
            tp.setP(Integer.toBinaryString(i), 1);

            //atualiza o número do quadro
            tp.setnQuadro(Integer.toBinaryString(i), nquadro);

            //atualiza os algoritmos de substituição
            update(nquadro);

            //remove a página do swap
            float tamanhoquadro = (float) mp.getTamanhoQuadro();
            disco.removeSwap(tamanhoquadro/1024f);
        }

    }

    //metodo utilizado quando eh necessario trazer um novo quadro do processo sendo executado
    public String alocaQuadroNovo(String nPagina, boolean algsub, int id, Processador cpu, String[] inst){

        ArrayList<String> quadrosDisp = mp.getQuadrosDisp();

        //verifica se a memoria esta cheia(se ha quadros disponiveis)
        if(!quadrosDisp.isEmpty()){
            //se a memoria nao estiver cheia
            //aloca um quadro da memoria para a pagina nova
            int indice = Integer.parseInt(quadrosDisp.get(0), 2);
            String retorno = quadrosDisp.get(0);
            mp.alocaQuadro(indice, id + " " + nPagina);
            //remove a página do swap
            float tamanhoquadro = (float) mp.getTamanhoQuadro();
            disco.removeSwap(tamanhoquadro/1024f);

            //recupera o processo
            Processo p = cpu.getProcessoAtual();

            String[] instpos = new String[2];
            instpos[0] = inst[0];//Um evento I/O eh escrita
            instpos[1] = inst[1];//O endereco

            //bloqueia o processo
            //diz que o processo estava bloqueado
            p.setEstavaBloqueado(true);
            //a instrução que deve ser executada após o bloqueio é a mesma que foi executada atualmente,
            //porém, agora a página estará presente em memória
            p.setInstrucaoPosBloqueio(instpos);

            //coloca o status para bloqueado
            p.setStatus("bloqueado");

            //armazena o tempo em que foi o estado para bloqueado
            p.setUltimaTroca(System.currentTimeMillis());

            //retira o processo do processo atual
            cpu.setProcessoAtual(null);

            //coloca o tempo de bloqueio para 2 loops
            p.setTempoCastigo(2);

            //retorna o quadro que foi alocado
            return retorno;

        }else {

            //se não houver quadros disponíveis, aplica um algoritmo de substituição
            String quadrotrocado;

            //verifica qual é o algoritmo de substiruição adotado
            //true para lru e false para subclock
            //obtém o número do quadro que deve ser substituido
            if(algsub){
                quadrotrocado = subLRU();
            }else{
                quadrotrocado = subClock();  
            }

            //corrige a tabela de paginas do processo que teve sua pagina substituida

            int indice = Integer.parseInt(quadrotrocado, 2);
            //recupera id do processo e o número da página que estava no quadro que será
            //substituído
            String[] infoquadro = mp.getQuadro(indice);

            //encontra o processo ao qual pertencia o quadro substituido e coloca o bit de presença
            //da página que estava no quadro para 0
            for(Processo x : processos){
                if(x.getId() == Integer.parseInt(infoquadro[0])){
                    TabeladePaginas tp = x.getTp();
                    tp.setP(infoquadro[1], 0);
                    break;
                }

            }

            //invalida a entrada referente a pagina que está sendo substituida na tlb, se estiver lá.

            cpu.setValid0(infoquadro[1], Integer.parseInt(infoquadro[0]));

            //aloca o quadro para a nova página
            mp.alocaQuadro(indice, id + " " + nPagina);

            //atualizacao do status do processo atual, que precisou de novo quadro
            //recupera o processo
            Processo p = cpu.getProcessoAtual();

            //obtém a última instrução que o processo executou, ou seja, a instrução que está sendo exe
            //cutada no momento
            String[] instpos = new String[2];
            instpos[0] = inst[0];//Um evento I/O eh escrita
            instpos[1] = inst[1];//O endereco

            //bloqueia o processo
            //diz que o processo estava bloqueado
            p.setEstavaBloqueado(true);
            //a instrução que deve ser executada após o bloqueio é a mesma que foi executada atualmente,
            //porém, agora a página estará presente em memória
            p.setInstrucaoPosBloqueio(instpos);

            //coloca o tempo de bloqueio para 2 loops
            p.setTempoCastigo(2);

            //coloca o status para bloqueado
            p.setStatus("bloqueado");

            //armazena o tempo em que foi o estado para bloqueado
            p.setUltimaTroca(System.currentTimeMillis());

            //retira o processo do processo atual
            cpu.setProcessoAtual(null);

            //retorna o quadro que foi trocado
            return quadrotrocado;
        }

    }

    //função que tira completamente um processo da memória
    public void tiraProcesso(int id){

        //itera por todos os quadros da memória procurando quadros que estejam, alocados para o]
        //processo a ser retirado, ao encontrar um quadro, o libera
        int i, max = mp.getQntdQuadros();
        for(i = 0; i < max; i++){
            String[] aux = mp.getQuadro(i);
            if(Integer.parseInt(aux[0]) == id){
                mp.liberaQuadro(i);
                //adiciona a página no swap
                float tamanhoquadro = (float) mp.getTamanhoQuadro();
                disco.adicionaSwap(tamanhoquadro/1024f);
            }
        }

    }

    public void removeProcesso(int id){
        for(Processo x : processos){
            if(x.getId() == id){
                processos.remove(x);
                break;
            }
        }
    }

    //RECEBE MEMORIA ALOCADA DE UM PROCESSO EM MB
    public float getMemAlocada(int id){
        String[] quadros = mp.getQuadros();
        float memAlocada = 0;

        for (String x : quadros){
            String[] aux = x.split(" ");
            if(Integer.parseInt(aux[0]) == id){
                float tamanhoquadro = (float) mp.getTamanhoQuadro();
                memAlocada += tamanhoquadro/1024f;
            }
        }
        return memAlocada;
    }


}
