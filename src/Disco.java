public class Disco {

    //Tamanho total do swap, em MB
    private float tamswap;

    //Quantidade de swap no disco, em MB
    private float swapocupado;

    public Disco(int tamdisco){

        this.tamswap = tamdisco;
        this.swapocupado = 0;

    }

    public float getTamSwap(){
        return this.tamswap;
    }

    public float getSwapocupado(){
        return this.swapocupado;
    }
    
    //Metodo para adicionar processo no swap, retorna false caso nao ha espaco disponivel
    public boolean adicionaSwap(float quantidade){

        if (swapocupado + quantidade > tamswap){
            return false;
        }

        swapocupado += quantidade;
        return true;

    }

    //Metodo para remover processo no swap, retorna false caso tente-se retirar mais que o que foi ocupado
    public boolean removeSwap(float quantidade){

        if(swapocupado - quantidade < 0){
            return false;
        }

        swapocupado -= quantidade;
        return true;

    }


}
