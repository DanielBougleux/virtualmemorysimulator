import java.io.*;
import java.util.ArrayList;
import java.math.*;

//classe reponsável pela temporização da execução e pela interação entre as classes, fazendo a simulação.
public class Controlador {

    //variável que representa o escalonador de processos
    private Escalonador escalonador;

    //variável que representa um gerenciador de memória
    private GerenciadordeMemoria gerenciadordeMemoria;

    //variável que representa a cpu
    private Processador cpu;

    //variável que representa o disco
    private Disco disco;

    //Variável que guarda o tamanho da memória principal em megabytes
    private int tamanhoMemoria;

    //variável que guarda o tamanho do quadro em kbytes
    private int tamanhoQuadro;

    //variável que guarda o tamanho do endereço lógico em bits
    private int tamanhoEnderecoLogico;

    //variável que guarda o tamanho do endereço físico em bits
    private int tamanhoEnderecoFisico;

    //variável que guarda a quantidade de processos a serem executados
    private int qntdProcessos;

    //variavel para implementacao da da interface grafica, mas dizer qual foi o ultimo endereco
    private String ultimoEndAcessado;

    //variavel para identificar o algoritmo de substituicao a ser utilizado, true = LRU, false = Clock com bit u
    private boolean algsub;

    //guarda o número de bits do endereço lógico que é referente ao número da página
    private int bitsNpagina;

    //lista que guarda as informações necessária para a criação de cada processo a ser executado
    private ArrayList<String[]> processos;

    //Guarda a variável que representa a interface
    private panelController painel_principal;

    //método que cria uma classe controlador passando as configurações do simulador como parâmetro
    //recebe o tamanho da memória principal, o tamanho do endereço lógico, o tamanho do quadro, o
    //nome do arquivo de onde virão as informações sobre cada processo criado
    // , o tamanho da tlb , o tamanho do disco em megabytes e o modo da politica de substituicao
    //se for true eh usado LRU se for false eh usado Clock com bit u
    public Controlador(int tamanhoMemoria, int tamanhoEnderecoLogico,
                       int tamanhoQuadro, String arquivo, int tamanhoTLB, int tamanhodisco, boolean algsub){

        //calcula o tamanho da memória em kbytes
        int temp = tamanhoMemoria * 1024;

        //incializa a quantidade de processos
        qntdProcessos = 0;

        painel_principal = null;

        if(tamanhoQuadro > temp){
            System.out.println("O tamanho do quadro excede a memória");
            System.exit(1);
        }

        ultimoEndAcessado = null;

        this.algsub = algsub;

        //inicializa o array de processos
        processos = new ArrayList<>();

        //guarda o tamanho do endereço lógico
        this.tamanhoEnderecoLogico = tamanhoEnderecoLogico;

        int k, potencia = 2;
        //calcula o valor de dois elevado a tamanho do endereco logico(quantidade máxima de bytes)
        for(k = 1; k<tamanhoEnderecoLogico; k++){
            potencia *= 2;
        }

        //calcula o tamanho máximo de um processo em kbytes
        potencia /= 1024;

        int tamanhomax;

        if(potencia % tamanhoQuadro == 0){
            tamanhomax = potencia/tamanhoQuadro;
        }else {
            tamanhomax = potencia/tamanhoQuadro + 1;
        }

        int l, bitslog = 1;
        for(l = 2; l < tamanhomax; l *= 2) {
            bitslog++;
        }

        bitsNpagina = bitslog;

        //verifica se o tamanho da memória principal é um múltiplo do tamanho do quadro o que é um dos
        //requisitos do projeto
        if(temp % tamanhoQuadro == 0){

            //guarda o tamanho da memória
            this.tamanhoMemoria = tamanhoMemoria;

            //guarda o tamanho do quadro
            this.tamanhoQuadro = tamanhoQuadro;

            //este for verifica quantos bits são necessários para o endereço físico, verificando qual é a
            //menor potência de 2 que é maior que a memória
            int j, bits = 1;
            for(j = 2; j < tamanhoMemoria; j *= 2) {
                bits++;
            }
            //soma 20 bit para transformar em megabytes
            this.tamanhoEnderecoFisico = bits + 20;
        }else {
            //caso a condição não seja satisfeita, aborta a execução
            System.out.println("ERRO<TAMANHO DA MEMÓRIA NÃO É MÚLTIPLO DO TAMANHO DO QUADRO>");
            System.exit(1);
        }

        //inicializa a cpu
        cpu = new Processador(tamanhoQuadro, tamanhoTLB);

        //inicializa o disco
        disco = new Disco(tamanhodisco);

        //incializa o escalonador
        escalonador = new Escalonador(cpu);

        //incializa o gerenciador de memória
        gerenciadordeMemoria = new GerenciadordeMemoria(tamanhoQuadro, tamanhoMemoria, disco);

        //abre o arquivo de criação dos processos, criando as variáveis necessárias
        InputStream is = null;

        try {
            is = new FileInputStream(arquivo);
        }catch (FileNotFoundException e){
            System.out.println("ARQUIVO DE PROCESSOS NÃO ENCONTRADO, ABORTANDO EXECUÇÃO...");
            System.exit(1);
        }

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader br = new BufferedReader(isr);

        String aux;

        //enquanto houverem linhas para serem lidas:
        while (true){
            try {
                //lê uma nova linha
                aux = br.readLine();
                //verifica se o arquivo acabou
                if (aux == null) break;
                //separa a linha em pedaços
                this.processos.add(aux.split(" "));
                //soma um a quantidade de processos
                this.qntdProcessos ++;
            }catch (IOException i){
                //se houver um erro ao ler o arquivo, aborta a execução
                System.out.println("Erro ao ler arquivo");
                i.printStackTrace();
                System.exit(1);
            }
        }

    }

    //função que executa uma etapa completa do simulador, passando por todas as funções
    public void loop(){

        System.out.println("Tempo atual: " + cpu.getTempoAcumulado());
        boolean submete = submeteProcesso();
        verificaTimeSlice();
        desbloqueiaProcesso();
        if(!escalonador.vazia()){
            if(cpu.getProcessoAtual() == null){
                escalonador.colocaEmExecucao();
            }
        }
        Processo processoAtual = cpu.getProcessoAtual();
        cpu.buscaInstrucao();
        String[] instrucaoAtual = cpu.getInstrucaoAtual();
        if(instrucaoAtual != null && instrucaoAtual[0] != null){
            interpreta(instrucaoAtual);
        }else if(instrucaoAtual == null){
            if(escalonador.vazia() && processos.isEmpty()){
                System.exit(1);
            }
            gerenciadordeMemoria.tiraProcesso(processoAtual.getId());
            gerenciadordeMemoria.removeProcesso(processoAtual.getId());
            cpu.invalidaProcesso(processoAtual.getId());
            cpu.setProcessoAtual(null);
        }
        //verifica se houve uma liberação de memória para trazer processos da fila de pronto/suspenso
        if(!gerenciadordeMemoria.memCheia() && !escalonador.getFilaSuspenso().isEmpty()){
            ArrayList<Processo> filaSuspenso = escalonador.getFilaSuspenso();
            for(Processo x : filaSuspenso){
                if(x.getStatus().equalsIgnoreCase("pronto/suspenso")){
                    gerenciadordeMemoria.alocaQuadrosIniciais(x.getId());
                    x.setStatus("pronto");
                    escalonador.adicionaProcesso(x);
                }
            }
        }
        verificaSwapper();
        int timeslice = escalonador.getTimeslice();
        timeslice++;
        escalonador.updateTimeSlice(timeslice);
        cpu.updateTime();
        attPainel();
        attListaProcesso();
        attPageTable();
        attTableQuadros();
        attTLBtable();
        imprimeInfo();

    }

    //Função que verifica se algum processo deve ser submetido e o submete.
    private boolean submeteProcesso(){


        System.out.println("Verificando se há processos a serem submetidos...");

        //for que passa por todos as entradas de criação de processos verificando se já está na hora
        //do processo ser submetido, se estiver, guarda o índice da entrada
        int i, max = processos.size(), indice = -1;
        for(i = 0; i < max; i++){
            if(Integer.parseInt(processos.get(i)[2]) <= cpu.getTempoAcumulado()){
                indice = i;
                break;
            }
        }

        //verifica se há um processo a ser submetido
        if(indice != -1){

            System.out.println("Novo processo encontrado :" + processos.get(indice)[0]);

            //carrega as informações do processo em uma variável
            String[] conf = processos.get(indice);

            System.out.println("verificando se o processo cabe no sistema");

            //calcula o tamanho em bits do endereço lógico do processo
            int tamendlogprocesso = 1, tamanhoproc = Integer.parseInt(conf[1]);

            for(i = 2; i < tamanhoproc; i *= 2) {
                tamendlogprocesso++;
            }

            //se o processo tiver tamanho 0, é um processo incompatível
            if(Integer.parseInt(conf[1]) < 1){
                System.out.println("Processo com tamanho imcompativel");
                processos.remove(indice);
                return false;
            }

            tamendlogprocesso += 20;//Soma mais vinte pois o tamanho total eh em MB


            //verifica se o endereço lógico do processo é condizente com o e endereço lógico máximo do sistema
            if(tamendlogprocesso < tamanhoEnderecoLogico){

                System.out.println("Verificando o id do processo...");

                //verifica se já existe um processo com o mesmo id
                for(Processo x : gerenciadordeMemoria.getProcessos()){
                    if(x.getId() == Integer.parseInt(conf[0])){
                        //se já existir esse processo, aborta a criação
                        System.out.println("Já existe um processo com o mesmo id");
                        processos.remove(indice);
                        return false;
                    }
                }

                System.out.println("ID ok");

                System.out.println("Verificando se há memória disponível...");

                //verifica se há memória suficiente para trazer as primeiras páginas do processo para a MP
                if(!gerenciadordeMemoria.memCheia()){
                    //se houver
                    System.out.println("Há memória disponível, criando processo e adicionando à fila de pronto");
                    
                    //tenta adicionar o processo no disco
                    boolean temdisco = disco.adicionaSwap(Integer.parseInt(conf[1]));
                    
                    //se nao houver espaco no disco, o processo nao eh criado
                    if(!temdisco){
                        return false;
                    }

                    //se houver espaco no disco cria o processo
                    Processo aux = new Processo(conf[3], Integer.parseInt(conf[0]),
                            Integer.parseInt(conf[1]), tamanhoQuadro, bitsNpagina);

                    //adiciona o processo na lista de processos
                    gerenciadordeMemoria.adicionaProcesso(aux);
                    //aloca 4 quadros iniciais para o processo
                    gerenciadordeMemoria.alocaQuadrosIniciais(aux.getId());
                    //seta o status para pronto
                    aux.setStatus("pronto");
                    //guarda o tempo da última troca de estado
                    aux.setUltimaTroca(System.currentTimeMillis());
                    //adiciona o processo na fila
                    escalonador.adicionaProcesso(aux);
                    

                }else {

                    //caso não haja memória suficiente
                    System.out.println("Memória insuficiente, movendo para pronto/suspenso");
                    //move o processo para pronto suspenso

                    boolean mem = disco.adicionaSwap(Integer.parseInt(conf[1]));

                    //verifica se ainda há espaço em disco
                    if (mem){
                        //se houver, cria o processo
                        Processo aux = new Processo(conf[3], Integer.parseInt(conf[0]),
                                Integer.parseInt(conf[1]), tamanhoQuadro, bitsNpagina);
                        //adiciona na lista de processos
                        gerenciadordeMemoria.adicionaProcesso(aux);
                        //muda o estado para pronto suspenso
                        aux.setStatus("pronto/suspenso");
                        //guarda o tempo da troca de estado
                        //aux.setUltimaTroca(System.currentTimeMillis());
                        //adiciona o processo a fila de pronto/suspenso
                        escalonador.adicionaProntoSuspenso(aux);
                    }else {
                        //se não houver swap disponível, houve erro de memória
                        System.out.println("ERRO<SEM MEMÓRIA>");
                        System.exit(1);//Aborto toda a execucao
                    }
                }

            }else {
                //caso o endereço lógico do processo seja maior que o do sistema, ocorre um erro
                System.out.println("ERRO<O PROCESSO NÃO CABE NO SISTEMA>");
                processos.remove(indice);
                return false;
            }



        }
        if(indice>=0){
            processos.remove(indice);//Retiro processo submetido
        }
        //retorna true se deu tudo certo
        return true;
    }

    //função reponsável pelo comportamento do time slice
    private void verificaTimeSlice(){

        if(cpu.getProcessoAtual() == null){
            escalonador.updateTimeSlice(0);
        }

        //verifica se já se passaram 5 unidades de tempo desde o último timeslice
        if(escalonador.getTimeslice() >= 5){

            //se o a fila de processsos não está vazia
            if(!escalonador.vazia()){
                //tira o processo de execução
                Processo aux = cpu.getProcessoAtual();
                if(aux != null){
                    //coloca o processo no final da fila
                    escalonador.adicionaProcesso(aux);

                    //atualiza o status para pronto
                    aux.setStatus("pronto");

                    //coloca o primeiro da fila em execução
                    escalonador.colocaEmExecucao();
                    //zera o time slice
                    escalonador.updateTimeSlice(0);
                }
            }

        }

    }

    //Metodo que simula um acesso a memoria
    private boolean acessaMemoria(int enderecoLogico, boolean modifica, String[] inst){
        
        Processo p = cpu.getProcessoAtual();//Recebe o processo que esta sendo executado

        int tamanhoprocesso = p.getTamanhoproc(), i, potencia = 2;//Guarda o tamanho total do processo, em MB
        //calcula o valor de dois elevado a 20
        for(i = 1; i<20; i++){
            potencia *= 2;
        }
        //calcula o tamanho do processo em bytes
        tamanhoprocesso *= potencia;
        //verifica se o endereco que esta tentando ser acessado eh maior do que o limite do processo
        //ou se o endereco logico eh negativo
        if(tamanhoprocesso < enderecoLogico || enderecoLogico < 0){
            return false;
        }

        String endBin = Integer.toBinaryString(enderecoLogico);//Guardo o endereco logico em binario
        int max = tamanhoEnderecoLogico - endBin.length(), cont;
        for(cont = 0; cont < max; cont++){//se o endereco logico nao possui o tamanho certo
            endBin = "0" + endBin;//adiciona zero para ter a quantidade de bits correta
        }
        String nPagina = endBin.substring(0, p.getBitsNPagina());//Separo do enderoco logico os bits de id da pagina

        //faz uma conversão e depois uma reconversão para cortar os 0s a esquerda
        int auxcnv = Integer.parseInt(nPagina, 2);

        nPagina = Integer.toBinaryString(auxcnv);

        ElementoTabelaPaginas elemtlb = cpu.buscaTlbAtual(nPagina);//pego na tlb a pagina identificada no endereco logico

        //se o elemento da tabela de paginas ja estiver na tlb 
        if(elemtlb != null){
            //calcula o endereco real e armazena
            ultimoEndAcessado = cpu.calcEndReal(endBin, elemtlb.getnQuadro(), p.getBitsNPagina());
            gerenciadordeMemoria.update(elemtlb.getnQuadro());
        }else{
            //se o elemento da tabela de paginas nao estiver na tlb
            //busca o elemento na tabela de paginas
            TabeladePaginas tp = p.getTp();
            int indice = Integer.parseInt(nPagina, 2);
            ElementoTabelaPaginas elemtp = tp.getElem(indice);

            //verifica o bit de presenaca da pagina em memoria
            int presenca = elemtp.getP();

            if(presenca == 1){
                //se a pagina estiver em memoria
                //calcula o endereco real
                ultimoEndAcessado = cpu.calcEndReal(endBin, elemtp.getnQuadro(), p.getBitsNPagina());
                //verifica se a operacao foi de escrita
                if(modifica){
                    //se foi seta o bit de modificado para 1
                    elemtp.setM(1);
                }
                //adiciona o elemento na tlb
                cpu.addtlb(p.getId(), nPagina, elemtp);

                gerenciadordeMemoria.update(elemtp.getnQuadro());

            }else{//A pagina nao esta na memoria
                
                int pageFault = p.getPageFault();
                pageFault++;
                p.setPageFault(pageFault);//atualiza o atributo de page fault do processo atual
                String nquadro = gerenciadordeMemoria.alocaQuadroNovo(nPagina, algsub, p.getId(), cpu, inst);//recebo o quadro da pagina
                //atualizo as informacoes necessarias
                elemtp.setP(1);
                elemtp.setnQuadro(nquadro);
                gerenciadordeMemoria.update(nquadro);

            }
        }

        return true;
    }
    //funcao para definir as acoes de acordo com as flags dos comandos
    private void interpreta(String[] inst){

        if(inst[0].equalsIgnoreCase("P")){

            int endlog = Integer.parseInt(inst[1]);
            boolean sucesso = acessaMemoria(endlog, false, inst);//false indica que nao sera atualizado o bit M na TP
            if(!sucesso){//a funcao acessaMemoria retorna false caso o endereco logico seja invalida, aqui essa situacao eh tratada
                //aborta o processo, invalidando as paginas da tlb e removendo da cpu
                Processo p = cpu.getProcessoAtual();
                cpu.invalidaProcesso(p.getId());
                cpu.setProcessoAtual(null);
            }

        }else if(inst[0].equalsIgnoreCase("R")){//Como na simulacao o P e R sao indeferentes, P e R possuem os mesmos comandos
                                                //Mas foram colocados em if diferentes caso seja necessaria a adicao de algo
            int endlog = Integer.parseInt(inst[1]);
            boolean sucesso = acessaMemoria(endlog, false, inst);
            if(!sucesso){
                //aborta o processo, invalidando as paginas da tlb e removendo da cpu
                Processo p = cpu.getProcessoAtual();
                cpu.invalidaProcesso(p.getId());
                cpu.setProcessoAtual(null);
            }

        }else if(inst[0].equalsIgnoreCase("W")){

            int endlog = Integer.parseInt(inst[1]);
            boolean sucesso = acessaMemoria(endlog, true, inst);//true para que seja atualizado o bit M na TP
            if(!sucesso){
                //aborta o processo, invalidando as paginas da tlb e removendo da cpu
                Processo p = cpu.getProcessoAtual();
                cpu.invalidaProcesso(p.getId());
                cpu.setProcessoAtual(null);
            }

        }else if(inst[0].equalsIgnoreCase("I")){

            Processo p = cpu.getProcessoAtual();
            p.setEstavaBloqueado(true);//Como o processo precisa aguardar o evento I/O, comeca o seu bloqueio
            String[] instpos = new String[2];
            instpos[0] = "W";//Um evento I/O eh escrita
            instpos[1] = inst[1];//O endereco
            p.setInstrucaoPosBloqueio(instpos);//Guardo a instrucao que sera utilizada apos o evento no formato [flag, EndLog]
            p.setStatus("bloqueado");
            p.setUltimaTroca(System.currentTimeMillis());//Atualizo o tempo onde foi trocado
            cpu.setProcessoAtual(null);//Tiro o processo atual da memoria
            p.setTempoCastigo(5);//o castigo por 4, considerando que o tempo de acesso a disco seria 1

        }else{//Caso a flag nao seja valida, aborta o processo

            Processo p = cpu.getProcessoAtual();
            gerenciadordeMemoria.tiraProcesso(p.getId());
            cpu.invalidaProcesso(p.getId());
            cpu.setProcessoAtual(null);

        }

    }

    //funcao para atualizar todos os processos que nao estejam prontos ou executando
    //a atualizao eh feita com base no tempoCastigo
    //Se seu tempo de castigo acabou, o processo deixa de ficar bloqueado
    private void desbloqueiaProcesso(){

        ArrayList<Processo> processos = gerenciadordeMemoria.getProcessos();
        int tempoCastigo;
        for(Processo x : processos){
            if(x.getStatus().equalsIgnoreCase("bloqueado/suspenso")){
                tempoCastigo = x.getTempoCastigo();
                tempoCastigo--;
                if(tempoCastigo <= 0){
                    tempoCastigo = 0;
                    x.setStatus("pronto/suspenso");
                    x.setEstavaBloqueado(true);
                }
                x.setTempoCastigo(tempoCastigo);
            }else if(x.getStatus().equalsIgnoreCase("bloqueado")){
                tempoCastigo = x.getTempoCastigo();
                tempoCastigo--;
                if(tempoCastigo <= 0){
                    tempoCastigo = 0;
                    x.setStatus("pronto");
                    x.setEstavaBloqueado(true);
                    escalonador.adicionaProcesso(x);
                }
                x.setTempoCastigo(tempoCastigo);
            }
        }

    }

    //Metodo para achar o processo a ser retirado do swapper
    private void verificaSwapper(){

        Processo p = null;
        ArrayList<Processo> filaSuspenso = escalonador.getFilaSuspenso();
        for(Processo x : filaSuspenso){//Laco para achar o processo
            if(x.getStatus().equalsIgnoreCase("pronto/suspenso")){
                p = x;
                break;
            }
        }

        if(p != null){//Se foi achado um processo pronto/suspenso
            Processo proc = escalonador.swapper(gerenciadordeMemoria.getProcessos());//proc recebe o processo a ser retirado do swapper
                                                                                    //null se o metodo nao encontrou processo bloqueado 
            if(proc != null){//foi encontrado o processo a ser retirado

                gerenciadordeMemoria.tiraProcesso(proc.getId());
                proc.setStatus("bloqueado/suspenso");
                int tempoCastigo = proc.getTempoCastigo();
                tempoCastigo += 4;//Ja que o processo sera retirado, o castigo um pouco mais devido a a retirada de suas paginas
                proc.setTempoCastigo(tempoCastigo);
                proc.setUltimaTroca(System.currentTimeMillis());
                proc.getTp().zeraP();
                escalonador.adicionaProntoSuspenso(proc);
                cpu.invalidaProcesso(proc.getId());
                //depois do processo proc ser retirado, aloco o novo processo p
                System.out.println(p.getTp());
                gerenciadordeMemoria.alocaQuadrosIniciais(p.getId());
                p.setStatus("pronto");
                escalonador.adicionaProcesso(p);
                escalonador.removeSuspenso(p.getId());

            }
        }



    }

    //Metodo para impressao de informacoes importantes, sem a interface grafica
    private void imprimeInfo(){
        //Impressao de todos os processos e seus estados
        ArrayList<Processo> processos = gerenciadordeMemoria.getProcessos();
        Processo atual = null;
        System.out.println("=====Lista de todos os processos=====");
        for(Processo p : processos){
            System.out.printf("\nID: %d Status: %s", p.getId(), p.getStatus());
            if(p.getStatus().equalsIgnoreCase("executando")){
                atual = p;
            }
        }
        System.out.println("");
        //Impressao do processo atual
        if(cpu.getProcessoAtual() != null){
            if(atual.getId() == cpu.getProcessoAtual().getId()){
                System.out.printf("Processo atual: %d\n", atual.getId());
                System.out.printf("Paginas do processo: %d\n", atual.getTamanhotp());
            }else{
                System.out.println("O processo atual e o processo executando nao sao os mesmos");
            }
        }
        //Instrucao atual
        System.out.println("==Instrucao atual==");
        String[] instAtual = cpu.getInstrucaoAtual();
        if(instAtual != null){
            System.out.printf("%s %s\n", instAtual[0], instAtual[1]);
        }
        //Ultimo endereco acessado
        System.out.println("==Ultimo endereco acessado==");
        System.out.println(this.ultimoEndAcessado);
        System.out.println("=====Fila de processos prontos====");
        ArrayList<Processo> filaPronto = escalonador.getFilaProcessos();
        for(Processo p : filaPronto){
            try {
                System.out.printf("ID: %d Status: %s \n", p.getId(), p.getStatus());
            } catch (Exception e) {
            }
        }
        System.out.println("=====Fila de processos suspensos====");
        ArrayList<Processo> filaSuspenso = escalonador.getFilaSuspenso();
        for(Processo p : filaSuspenso){
            System.out.printf("ID: %d Status: %s \n", p.getId(), p.getStatus());
        }
        System.out.println("=====Disco=====");
        System.out.printf("Swap: %f MB\nSwap Ocupado: %f MB\n", disco.getTamSwap(), disco.getSwapocupado());
        System.out.println("=====Memoria Principal=====");
        /*
        System.out.println("==Quadros Disponiveis==");
        ArrayList<String> quadrosDisp = gerenciadordeMemoria.getQuadrosDisp();
        for(String s : quadrosDisp){
            System.out.println(s);
        }
        */
        System.out.println("==Quadros==");
        String[] quadros = gerenciadordeMemoria.getQuadros();
        for(String s : quadros){
            if(s != "-1"){
                System.out.println(s);
            }
        }
        System.out.println("=====TLB=====");
        ElementoTlb[] elemtlb = cpu.getElementosTlb();
        for(ElementoTlb elem : elemtlb){
            try{
                System.out.printf("Valid: %d ID: %d Pagina: %s\n", elem.getValid(), elem.getId(), elem.getnPagina());
            } catch (Exception e){}
            
        }
    }

    //SETTER PAINEL PRINCIPAL
    public void setPainelPrincipal(panelController painel) {
        painel_principal = painel;
    }

    //FUNÇÃO QUE ATUALIZA FUNÇÕES DO PAINEL
    public void attPainel() {
        //Atualiza quantidade de quadros livres
        painel_principal.setQuadrosLivres(Integer.toString(gerenciadordeMemoria.getQuadrosDisp().size()));
        //Atualiza quantidade de páginas alocadas na memória
        painel_principal.setPaginasAlocadas(Integer.toString(
                gerenciadordeMemoria.getQuadros().length - gerenciadordeMemoria.getQuadrosDisp().size()));
        //Atualiza último endereço acessado
        painel_principal.setUltimoEndereco(ultimoEndAcessado);
        //Atualiza disco usado
        painel_principal.setDiscoUsado(Float.toString(disco.getSwapocupado()));
        //Atualiza disco livre
        painel_principal.setDiscoLivre(Float.toString(
                disco.getTamSwap()-disco.getSwapocupado()));
        //Caso haja processo executando
        if(cpu.getProcessoAtual() != null) {
            //Atualiza processo em execução
            painel_principal.setProcessoExecucao(Integer.toString(cpu.getProcessoAtual().getId()));
            //Atualiza contador de page faults
            painel_principal.setPageFault(Integer.toString(cpu.getProcessoAtual().getPageFault()));
            //Atualiza memória alocada no momento do processo
            painel_principal.setMemAlocadaProcesso(Float.toString(
                    gerenciadordeMemoria.getMemAlocada(cpu.getProcessoAtual().getId())));
            //Atualiza memória total do processo
            painel_principal.setMemTotal(Integer.toString(cpu.getProcessoAtual().getTamanhoproc()));
            //Atualiza último instrução lida
            painel_principal.setUltimaInstrucao(
                    cpu.getInstrucaoAtual()[0] + cpu.getInstrucaoAtual()[1]);
        }
        //Caso não haja processo executando
        else {
            painel_principal.setProcessoExecucao("IDLE");
            painel_principal.setPageFault("0");
            painel_principal.setMemAlocadaProcesso("0");
            painel_principal.setMemTotal("0");
            painel_principal.setUltimaInstrucao("XX 00");
        }

    }

    //Atualiza lista de processos
    public void attListaProcesso() {
        painel_principal.clearProcessTable();
        ArrayList<Processo> processos = gerenciadordeMemoria.getProcessos();
        for(int i=0; i< processos.size(); i++) {
            painel_principal.addProcessTable(Integer.toString(processos.get(i).getId()),
                    processos.get(i).getStatus());
        }
    }

    //Atualiza tabela de páginas
    public void attPageTable() {

        painel_principal.clearPageTable();

        if(cpu.getProcessoAtual() != null){
            TabeladePaginas page_table = cpu.getProcessoAtual().getTp();
            ElementoTabelaPaginas[] linhas = page_table.getElemTabelaPag();
            for(int i =0; i<linhas.length; i++) {
                painel_principal.addPageTable(Integer.toString(linhas[i].getP()),
                        Integer.toString(linhas[i].getM()), Integer.parseInt(linhas[i].getnQuadro(), 2));
            }
        }

    }

    //Atualiza tabela de quadros
    public void attTableQuadros() {
        String[]quadros = gerenciadordeMemoria.getQuadros();
        painel_principal.clearFramesMP();
        for(int i =0; i<quadros.length; i++) {
            painel_principal.addFramesMP(Integer.toString(i), quadros[i].split(" ")[0]);
        }
    }

    //Atualiza tabela da TLB
    public void attTLBtable() {
        painel_principal.clearTLBtable();
        ElementoTlb[] linhas = cpu.getElementosTlb();
        for(int i =0; i<linhas.length; i++) {
            if (linhas[i] != null) {
                painel_principal.addTLBtable(Integer.toString(linhas[i].getValid()),
                        Integer.toString(linhas[i].getId()), Integer.toString(Integer.parseInt(linhas[i].getnPagina(), 2)),
                        Integer.toString(Integer.parseInt(linhas[i].getElementoPagina().getnQuadro())),
                        Integer.toString(linhas[i].getElementoPagina().getM()),
                        Integer.toString(linhas[i].getElementoPagina().getP()));
            }
        }
    }


}
