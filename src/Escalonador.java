import java.util.ArrayList;

//Classe responsável por escolher qual será o próximo processo a ser executado
//deve ser implementada uma política bem simples
public class Escalonador {

    //Mantém uma fila de processos no estado pronto
    private ArrayList<Processo> filaProcessos;

    //Mantém uma fila de processos no estado pronto/suspenso
    private ArrayList<Processo> filaSuspenso;

    //Variável que mantém o processador que está sendo usado no sistema
    private Processador cpu;


    //Variavel para manter o timeslice do processo atual
    private int timeslice;

    //Cria um escalonador dado um processador
    public Escalonador(Processador cpu){

        this.cpu = cpu;
        this.timeslice = 0;
        filaProcessos = new ArrayList<>();
        filaSuspenso = new ArrayList<>();

    }

    //Pega o primeiro da fila e coloca em execução na cpu
    public void colocaEmExecucao(){

        if(!vazia()){
            cpu.setProcessoAtual(filaProcessos.get(0));
            filaProcessos.get(0).setStatus("executando");
            filaProcessos.remove(0);
        }

    }

    //Adiciona um processo na fila de pronto
    public void adicionaProcesso(Processo processo){

        filaProcessos.add(processo);

    }

    //Adiciona um processo na fila de pronto-suspenso
    public void adicionaProntoSuspenso(Processo processo){
        filaSuspenso.add(processo);
    }

    //Verifica se a fila está vazia
    public boolean vazia(){

        return filaProcessos.isEmpty();

    }

    //Retorna toda a fila com processos suspenso
    public ArrayList<Processo> getFilaSuspenso(){
        return this.filaSuspenso;
    }

    //Função que aplica o algoritmo de swap. Se todos os processos estiverem bloqueados, escolhe o processo que
    //está bloqueado a mais tempo e retorna o seu id.
    public Processo swapper(ArrayList<Processo> processos){

        int i, max = processos.size(), swaped;
        for(i = 0; i < max; i++){
            //verifica se existe algum processo que nao esta bloqueado, se nao ha entao nao eh necessario continuar analisando o swapper
            if(!processos.get(i).getStatus().equalsIgnoreCase("BLOQUEADO") &&
            !processos.get(i).getStatus().equalsIgnoreCase("PRONTO/SUSPENSO") &&
            !processos.get(i).getStatus().equalsIgnoreCase("BLOQUEADO/SUSPENSO")){
                return null;
            }
        }

        swaped = 0;//Variavel para armazenar o indice do processo a ser trocado

        for(i = 0; i < max; i++){//laco para encontrar o processo trocado a mais tempo

            if (processos.get(i).getTempoUltimaTroca() > processos.get(swaped).getTempoUltimaTroca() &&
                    !processos.get(i).getStatus().equalsIgnoreCase("PRONTO/SUSPENSO") &&
                    !processos.get(i).getStatus().equalsIgnoreCase("BLOQUEADO/SUSPENSO")){
                swaped = i;//encontrei um com maior tempo
            }

        }

        return processos.get(swaped);

    }

    public void removeSuspenso(int id){
        for(Processo x : filaSuspenso){
            if(x.getId() == id){
                filaSuspenso.remove(x);
                break;
            }
        }
    }

    public int getTimeslice(){
        return timeslice;
    }

    //Metodo setTimeSlice, usado para aumentar o timeslice quando necessario
    public void updateTimeSlice(int timeslice){
        this.timeslice = timeslice;
    }

    //Retorna toda a lista de processos prontos
    public ArrayList<Processo> getFilaProcessos(){
        return filaProcessos;
    }

}
