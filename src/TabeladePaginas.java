
public class TabeladePaginas {

    //indica a qual processo a tabela de páginas pertence
    private int id;

    //vetor de elementos da tabela, que compõe a mesma
    private ElementoTabelaPaginas[] tabelaPaginas;

    //cria uma tabela de páginas de um determinado tamanho relacionada a um determinado processo
    public TabeladePaginas(int tamanhoTP, int id){

        //instancia as variáveis
        this.id = id;
        this.tabelaPaginas = new ElementoTabelaPaginas[tamanhoTP];
        //cria uma variável de iteração
        int i;
        //instancia cada elemento da tabela
        for(i = 0; i < tamanhoTP; i++){
            tabelaPaginas[i] = new ElementoTabelaPaginas();
        }

    }

    //abaixo estão os getters e setters relacionados aos elementos que uma tabela de páginas guarda. Em alguns casos
    //será necessária uma referência a getters e setters da classe elemento da tabela.

    public int getId() {
        return id;
    }

    public String getnQuadro(String nPagina){
        int temp = Integer.parseInt(nPagina, 2);
        return this.tabelaPaginas[temp].getnQuadro();
    }

    public int getM(String nPagina){
        int temp = Integer.parseInt(nPagina, 2);
        return this.tabelaPaginas[temp].getM();
    }

    public int getP(String nPagina){
        int temp = Integer.parseInt(nPagina, 2);
        return this.tabelaPaginas[temp].getP();
    }

    public void setM(String nPagina, int M){
        int temp = Integer.parseInt(nPagina, 2);
        this.tabelaPaginas[temp].setM(M);
    }

    public void setP(String nPagina, int P){
        int temp = Integer.parseInt(nPagina, 2);
        this.tabelaPaginas[temp].setP(P);
    }

    public void setnQuadro(String nPagina, String nQuadro){
        int temp = Integer.parseInt(nPagina, 2);
        tabelaPaginas[temp].setnQuadro(nQuadro);
    }

    public ElementoTabelaPaginas getElem(int index){
        return tabelaPaginas[index];
    }

    //função que seta todos os bits de presença de uma tabela de páginas para 0
    //essa função é utilizada quando um processo é tirado de memória
    public void zeraP(){
        for(ElementoTabelaPaginas elem : tabelaPaginas){
            if(elem != null){
                elem.setP(0);
            }
        }
    }

    public ElementoTabelaPaginas[] getElemTabelaPag(){
        return tabelaPaginas;
    }

}
