//elementos que compõe a TLB
public class ElementoTlb {

    //indica se aquele elemento da tabela é válido
    private int valid;

    //id do processo ao qual a linha se refere
    private int id;

    //número da página do processo a qual a linha se refere
    private String nPagina;

    //O elemento que foi carregado na TLB com suas devidas informações
    private ElementoTabelaPaginas elementoPagina;


    //cria uma linha da TLB
    public ElementoTlb(int id, String nPagina, ElementoTabelaPaginas elementoPagina){

        this.id = id;
        this.nPagina = nPagina;
        this.elementoPagina = elementoPagina;
        this.valid = 1;

    }

    //Retorna o id do processo, não é necessário um set pois, uma vez criada a linha, ela não pode se referir a um
    //processo diferente
    public int getId() {
        return id;
    }


    //retorna o numero da pagina, mesma coisa do anterior
    public String getnPagina() {
        return nPagina;
    }


    //retorna o elemento da TLB, mesma coisa do anterior
    public ElementoTabelaPaginas getElementoPagina() {
        return elementoPagina;
    }

    //funções que manipulam o bit de validade
    public int getValid(){
        return valid;
    }

    public void setValid(int valid){
        this.valid = valid;
    }

}
