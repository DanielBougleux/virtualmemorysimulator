import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Controlador controlador = new Controlador(1, 22, 64
        , "ArquivosTeste/TesteTimeSlice/criaProc.txt", 50, 50, true);

        Scanner teclado = new Scanner(System.in);

        while (true){
            controlador.loop();
            System.out.println("Deseja executar o proximo loop?(S/N)");
            String resp = teclado.next();
            if(resp.equalsIgnoreCase("N")){
                break;
            }
        }

        teclado.close();

    }


}
