import java.util.ArrayList;

//Classe que é responsável por guardar as configurações da memória principal
public class MemoriaPrincipal {

    //Guarda o tamanho da memória
    private int tamanhoMemoria;

    //Guarda o tamanho do quadro
    private int tamanhoQuadro;

    //Mantém uma lista de endereços dos quadros disponíveis em memória
    private ArrayList<String> quadrosDisp;

    //Mantém uma correlação de quadros para processos.
    //Se um um quadro i estiver alocado para um processo cujo id é j, o iésimo elemento de quadros será j
    //Caso um quadro não esteja alocado para nenhum processo, este guardará -1
    private String[] quadros;

    //Cria uma memória principal dadas as informações
    //O tamanho da memória é dado em megabytes e deve ser um múltiplo do tamanho do quadro que é dado em kbytes
    public MemoriaPrincipal(int tamanhoMemoria, int tamanhoQuadro){

        //calcula o tamanho da memória em kbytes
        int temp = tamanhoMemoria * 1024;

        this.tamanhoQuadro = tamanhoQuadro;

        //inicializa o vetor de quadros disponíveis
        quadrosDisp = new ArrayList<>();

        //calcula a quantidade de quadros em memória
        int loop = temp / tamanhoQuadro, i;

        //cria um vetor de quadros com todos os quadros
        quadros = new String[loop];

        //itera pelos quadros inicializando
        for(i = 0; i < loop; i++){
            //adiciona todos os quadros em quadros disponiveis
            quadrosDisp.add(Integer.toBinaryString(i));
            //inicialmente nenhum quadro está alocado para processo
            quadros[i] = "-1";
        }

    }

    //Funções que retornam as informações necessárias

    public int getTamanhoQuadro() {
        return tamanhoQuadro;
    }

    public int getTamanhoMemoria() {
        return tamanhoMemoria;
    }

    public ArrayList<String> getQuadrosDisp() {
        return quadrosDisp;
    }

    public int getMemDisp(){
        return quadrosDisp.size() * tamanhoQuadro;
    }

    public String[] getQuadro(int indice){
        return quadros[indice].split(" ");
    }

    public String[] getQuadros(){
        return quadros;
    }
    public int getQntdQuadros(){
        return quadros.length;
    }


    //função que aloca um quadro para um processo, a mensagem é id do processo + número da página
    public void alocaQuadro(int indice, String mensagem){
        //coloca a mensagem no quadro
        quadros[indice] = mensagem;
        //itera por quadros disponíveis até achar o quadro a ser alocado
        for (String x : quadrosDisp){

            if(Integer.parseInt(x, 2) == indice){
                //quando achar, remove o quadro
                quadrosDisp.remove(x);
                break;
            }
        }
    }

    //função que libera um quadro de memória
    public void liberaQuadro(int indice){
        //coloca o índice do quadro para -1, indicando que está livre
        quadros[indice] = "-1";
        //adiciona o quadro no vetor de quadros disponíveis
        quadrosDisp.add(Integer.toBinaryString(indice));
    }

}
