import java.util.Random;

//Classe que implementa a tlb, o processador deverá consultar essa classe para calcular o endereõ físico de um acesso
//a memoria
public class TLB {

    //A tlb possui um vetor de elemento da tlb que vai sendo populado ao longo da execução
    private ElementoTlb[] elementosTlb;

    //Guarda o tamanho da tlb para dizer quando deve ser feita uma substituição de elementos
    //O tamaho da TLB deve ser dado em quantidade de entradas
    private int tamanhoTlb;

    //informação necessária para manter a integridade da TLB
    private int tamanhoQuadro;

    //cria uma TLB de um determinado tamanho
    public TLB(int tamanhoTlb, int tamanhoQuadro){

        this.elementosTlb = new ElementoTlb[tamanhoTlb];
        this.tamanhoQuadro = tamanhoQuadro;
        this.tamanhoTlb = tamanhoTlb;

    }

    //retorna o tamamnho da tlb
    public int getTamanhoTlb(){
        return tamanhoTlb * tamanhoQuadro;
    }

    //retorna toda a tlb, usada primariamente para a impressao de informacoes do sistema
    public ElementoTlb[] getElementosTlb(){
        return this.elementosTlb;
    }

    //função que realiza uma consulta a tlb, dado um número da página e um id do processo. Retorna nulo caso dê miss
    public ElementoTabelaPaginas getElem(String nPagina, int processID){

        //percorre todos os elementos da tlb, inclusive os nulos
        for(ElementoTlb p : elementosTlb){
            //verifica se o elemento não é nulo
            if(p != null){
                //verifica se o elemento é o que está sendo procurado
                if(p.getId() == processID && p.getnPagina().equals(nPagina) && p.getValid() == 1){
                    //retorna o elemento
                    return p.getElementoPagina();
                }
            }
        }
        //se passar por todos os elementos e não encontrar nenhum, retorna nulo, ou seja, deu cache miss
        return null;

    }

    //Adiciona um elemento na TLB, também aplica a política de substituição aleatória de cache caso a tlb esteja cheia
    public void addElem(int processID, String nPagina, ElementoTabelaPaginas elem){

        //cria um novo elemento da tlb com as informações dadas
        ElementoTlb elemento = new ElementoTlb(processID, nPagina, elem);
        //cria uma variável de iteração
        int i;
        //percorre todos os elementos da tlb
        for(i = 0; i < this.tamanhoTlb; i++){
            //verifica se a entrada pode ser utilizada, ou seja, se ela não tem nada ou se a validade dela é 0
            if(this.elementosTlb[i] == null || this.elementosTlb[i].getValid() == 0){
                //se achar algum espaço insere o novo elemento e retorna
                elementosTlb[i] = elemento;
                return;
            }
        }

        //se não achar nenhum espaço, gera um índice aleatório e substitui a entrada daquele índice
        Random aleatorios = new Random();
        int sub = aleatorios.nextInt(this.tamanhoTlb);
        this.elementosTlb[sub] = elemento;


    }

    //função que invalida uma entrada relacionada a um processo
    public void setValid0(String nPagina, int id){

        //passa por todos os elementos da tlb
        for(ElementoTlb e : elementosTlb){
            //se o elemento não é nulo
            if(e != null){
                //verifica se é o elemento que está sendo procurado
                if(e.getId() == id && e.getnPagina().equalsIgnoreCase(nPagina)){
                    //se for, invalida o elemento
                    e.setValid(0);
                }
            }
        }
    }

    //essa função invalida todas as entradas de um
    // processo, esse metodo so eh utilizado quando o processo é tirado de memória
    public void invalidaProcesso(int id){

        //percorre todos os elementos da tlb
        for(ElementoTlb e : elementosTlb){

            //verifica se o elemento está associado ao processo terminado
            if(e != null && e.getId() == id){
                //invalida o elemento se estiver
                e.setValid(0);
            } 

        }
    }

}
