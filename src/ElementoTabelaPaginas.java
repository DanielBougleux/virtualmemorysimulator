public class ElementoTabelaPaginas {

    //bit de presença, indica se o elemento está em memória. Deve ser 0 ou 1
    private int p;

    //bit de modificado, indica se um elemento foi modificado. Auxilia na política de substituição.
    private int m;

    //Indica qual é o número do quadro ao qual esta página está relacionada. É uma string de bits.
    private String nQuadro;

    //cria um elemento da tabela de páginas com tudo zerado
    public ElementoTabelaPaginas(){
        this.p = 0;
        this.m = 0;
        this.nQuadro = "-1";
    }

    //abaixo são todos os getters e setters que farão a comunicação com as outras classes

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public String getnQuadro() {
        return nQuadro;
    }

    public void setnQuadro(String nQuadro) {
        this.nQuadro = nQuadro;
    }
}
