Observações :

   -> O arquivo de processos ao qual me refiro na classe controlador é um arquivo que define a criação
   de todos os processos que serão executados. o modelo do arquivo é o seguinte:
    -> cada linha do arquivo deverá possuir:
        -> ID do processo a ser criado
        -> Tamanho do processo em megabytes
        -> tempo no qual ele é submetido em unidades de tempo do simulador, para cada vez que o loop for execu
        tado adiciona-se uma unidade de tempo
        -> diretório do arquivo das informações do processo, que por sua vez irá conter cada
        instrução do processo, como foi especificado no site.

   -> Falta fazer a interpretação das instruções do processo que está sendo executado atualmente
            -> levar em consideração a TLB.
            -> uma instrução de IO deve conter também a duração da operação de IO

   -> no final verificar se há um processo para ser swaped out.
   ---